#ifndef CAN_DEVICE_H
#define CAN_DEVICE_H

#include <linux/can.h>
#include <linux/can/raw.h>
#include <stdint.h>
#include <stdlib.h>

#include <cstring>
#include <functional>
#include <iostream>

enum NODE_STATE {
  NODE_NOTINIT = 0,
  NODE_INIT_STATE,
  NODE_INITIALIZED,
  NODE_ACTIVE,
  NODE_ERROR,
  MAX_NODE
};

class CANDevice {
 private:
  uint16_t localNodeId;  // Internal node ID
  uint16_t masterState;  // Internal state for SYNC producer
  using canTXCallback = std::function<void(struct can_frame *)>;
  canTXCallback sendToCAN;

// CANopen protocol data transfer definations
#define SDO_W 0x600
#define SDO_R 0x580
#define SDO_23 0x23
#define SDO_2B 0x2b
#define SDO_2F 0x2f
#define SDO_40 0x40
#define PDO1_RX 0x180
#define PDO2_RX 0x280
#define PDO3_RX 0x380
#define PDO4_RX 0x480
#define PDO1_TX 0x200
#define PDO2_TX 0x300
#define PDO3_TX 0x400
#define PDO4_TX 0x500

 public:
  enum SYNCSTATE { SYNC_DISABLED = 0, SYNC_ENABLED = 1, SYNC_MAXENUM };
  CANDevice(uint16_t nodeid, SYNCSTATE state);
  virtual ~CANDevice();
  uint16_t getNodeId(void);  // getter for actual node id
  uint8_t deviceOnline;      // internal device link status
  can_frame canTx;           // can frame messages

  void bindCanTXCallback(canTXCallback cb, bool CAN_Enabled);
  void sendSDO(uint8_t cmd, uint16_t index, uint8_t sub_idx, uint32_t data);
  void sendPDO(uint16_t pdo_num, uint8_t *data);
  void sendNMT(uint8_t cmd);
  void sendSYNC(void);
  void sendRAW(can_frame &mes);

  virtual void deviceInit(void) = 0;
  virtual void deviceTimeTick(void) = 0;
  virtual void rxParse(struct can_frame *msg) = 0;
};

#endif