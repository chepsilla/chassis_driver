#ifndef CAN_SOCKET_H
#define CAN_SOCKET_H

#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <iostream>

class SocketCan {
 public:
  SocketCan();
  virtual ~SocketCan();
  bool sendExtendedId;
  using canRXCallback = std::function<void(struct can_frame *)>;
  using canTXCallback = std::function<void(void)>;
  void bindCanTXCallback(canTXCallback cb);
  void bindCanRXCallback(canRXCallback cb);

  int initInterface(std::string interface);
  void processCanTxEvent(const can_frame *msg);

  int initialized;

  uint16_t getErrorTx(void);
  uint16_t getErrorRx(void);

 private:
  int s;
  void canReadSome();
  void canRxHandler(const boost::system::error_code &error,
                    size_t bytes_transferred);
  boost::asio::io_service ioService;
  boost::asio::posix::stream_descriptor descriptor;

  uint16_t rxCallBackEna;
  canRXCallback rxCallBack;
  uint16_t txCallBackEna;
  canTXCallback txCallBack;

  uint16_t errorTx;
  uint16_t errorRx;

  can_frame canRxMsg;
  can_frame rx;
};

#endif