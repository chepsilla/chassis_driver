#ifndef CHASSIS_DRIVER_H
#define CHASSIS_DRIVER_H

#include <chassis_msgs/msg/chassis_cmd.hpp>
#include <chassis_msgs/msg/chassis_feedback.hpp>
#include <chassis_msgs/msg/motor_cmd.hpp>
#include <chassis_msgs/msg/motor_data.hpp>
#include <rclcpp/rclcpp.hpp>

#include "can/socket.hpp"
#include "devices/eld2_can.hpp"
#include "pse/can_pse.hpp"

using namespace chassis_msgs::msg;

constexpr uint16_t id_filter_mask = 0x1F;

class ChassisDriver : public rclcpp::Node {
 private:
  bool use_pse;
  std::string can_iface;
  uint16_t flId, frId, rlId, rrId;
  ELD2_CAN_data::ELD2_settings driverSettings;

  std::shared_ptr<SocketCan> socketCan;
  std::shared_ptr<PseCan> pseCan;
  std::shared_ptr<ELD2CAN> flELD2, frELD2, rlELD2, rrELD2;
  std::map<uint8_t, CANDevice *> rxCallbackMap;

  rclcpp::CallbackGroup::SharedPtr timerCbGroup;
  rclcpp::CallbackGroup::SharedPtr subCbGroup;

  rclcpp::TimerBase::SharedPtr timer;
  rclcpp::Publisher<ChassisFeedback>::SharedPtr feedbackPub;
  rclcpp::Subscription<ChassisCmd>::SharedPtr commandSub;

  ChassisFeedback chassisFeedback;

  template <class T>
  inline T declareAndGetParameter(std::string name);

  void readParameters();
  void initializeCANDevices();
  void canRxCallback(struct can_frame *msg);

  void timerCallback();
  void feedbackHelper(std::shared_ptr<ELD2CAN> drive, int id);
  void commandTypeHelper(const MotorCmd &cmd, std::shared_ptr<ELD2CAN> drive);
  void commandCallback(const ChassisCmd::SharedPtr msg);

 public:
  ChassisDriver();
  ~ChassisDriver();
};

#endif