#ifndef ELD2_CAN_H
#define ELD2_CAN_H

#include "can/device.hpp"
#include "devices/eld2_data.hpp"

using namespace ELD2_CAN_data;

class ELD2CAN : public CANDevice {
 public:
  ELD2CAN(uint16_t nodeid, SYNCSTATE state);
  ~ELD2CAN();

  void deviceCtrl(void);
  void setSpeed(uint32_t ref);
  void setLoopMode(uint8_t mode);
  NODE_STATE getState(void);
  void deviceInit(void);
  void deviceTimeTick(void);
  void rxParse(struct can_frame *msg);
  void rxSDOParse(struct can_frame *msg);
  void setCmdValue(uint16_t cmd);
  void setRefValue(float ref);
  void setDirValue(int dir);
  enum LOOP_MODE_STATE { POSITION = 1, SPEED = 3, TORQUE, MAX_ENUM_LOOP };
  void setLoopValue(uint8_t cmd);

  void setInfinityCommandMode();

  ELD2_data_st ELD2_data;
  ELD2_CAN_data::ELD2_settings settings;

 private:
  NODE_STATE nodeStatus;
  uint16_t initState;
  bool commandFlashBit;
  int cmdWaitCounter;

  enum INIT_PARAM_STATES {
    MAKE_NMT_OPERATIONAL = 0,
    SET_CONTROL_MODE_6060,
    SET_SPEED_LIMIT_607F,
    SET_ACCEL_LIMIT_6083,
    SET_ACCEL_LIMIT_6084,
    SET_ACCEL_LIMIT_6085,
    SET_PULSES_PER_REVOLUTION_2008,
    SET_HOMEBACK_MODE_6098,
    SET_HOMEBACK_SPEED_6099_1,
    SET_HOMEBACK_SPEED_6099_2,
    SET_HOMEBACK_ACCEL_609A,
    SET_HOMEBACK_OFFSET_607C,
    MAX_ENUM
  };

  uint16_t controlState;
  enum CONTROL_CMD_STATES {
    DISABLE = 0,
    GOTO_READY,
    CLEAR_ERROR,
    GOTO_ENABLED,
    GOTO_ACTIVE,
    GOTO_COMMAMD,
    MAX_ENUM_CONTROL
  };

  LOOP_MODE_STATE localLoop;
  LOOP_MODE_STATE getLoopValue(void);
  float localRef;
  float getRefValue(void);
  uint16_t localCmd;
  uint32_t getCmdValue(void);
  int localDir;
  bool getDirValue(void);

  ELD2_CAN_data::PDO1_TX_un PDO1_tx;
  ELD2_CAN_data::PDO2_TX_un PDO2_tx;
  ELD2_CAN_data::PDO3_TX_un PDO3_tx;
  ELD2_CAN_data::PDO4_TX_un PDO4_tx;
  ELD2_CAN_data::PDO1_RX_un PDO1_rx;
  ELD2_CAN_data::PDO2_RX_un PDO2_rx;
  ELD2_CAN_data::PDO3_RX_un PDO3_rx;
  ELD2_CAN_data::SDO_un SDO_tx;
  ELD2_CAN_data::SDO_un SDO_rx;
  void makePDO1();
  void makePDO2();
  void makePDO3();
  void makePDO4();
};

#endif