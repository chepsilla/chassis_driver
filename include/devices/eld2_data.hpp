#ifndef ELD2_DATA_H
#define ELD2_DATA_H

#include <stdint.h>
#define _USE_MATH_DEFINES
#include <math.h>

namespace ELD2_CAN_data {
constexpr float from_rad = 1.f / (2 * M_PI);
constexpr float min2sec = 1.f / 60;
constexpr uint32_t MOTOR_MAX_SPEED_RPM = 3000;
constexpr uint32_t ENCODER_PULSE = 10000;
constexpr uint32_t MAX_MOTOR_SPEED_PULSE =
    (MOTOR_MAX_SPEED_RPM * min2sec * ENCODER_PULSE);
constexpr uint32_t MAX_MOTOR_ACCEL = MAX_MOTOR_SPEED_PULSE;
constexpr float MOTOR_SPEED_SCALE = ENCODER_PULSE * min2sec;
constexpr float MOTOR_POSITION_SCALE = ENCODER_PULSE * from_rad;  // need fix
constexpr int CMDWAITCOUNTER = 20;
constexpr int CMDWAITINFINITY = -1;

struct ELD2_settings {
  uint32_t maxMotorSpeed;
  uint16_t maxTorquePercent;
  uint32_t maxAcceleration;
  uint32_t maxDeacceleration;
};

struct ELD2_data_st {
  union controlWordUn {
    struct {
      uint16_t switchOn : 1;
      uint16_t enableVoltage : 1;
      uint16_t quickStop : 1;
      uint16_t operationEnable : 1;
      uint16_t modeSpecific : 3;
      uint16_t flearFault : 1;
      uint16_t halt : 1;
    } controlWordSt;

    uint16_t controlWord;
  } control;

  union statusWordUn {
    struct {
      uint16_t readyOn : 1;
      uint16_t switchedOn : 1;
      uint16_t operationEnabled : 1;
      uint16_t fault : 1;
      uint16_t voltageEnaled : 1;
      uint16_t quickStop : 1;
      uint16_t switchOnDis : 1;
      uint16_t warning : 1;
      uint16_t manufactSpec : 1;
      uint16_t remote : 1;
      uint16_t targetReached : 1;

    } statusWordSt;

    uint16_t statusWord;
  } status;

  uint8_t actualMode;
  float refPosition;
  float fbPosition;
  float refSpeed;
  float fbSpeed;
  float refTorque;
  float fbTorque;
  uint16_t errorCode;
  uint16_t torqueLimitPercent;
};

union PDO1_TX_un {
  struct __attribute__((packed)) PDO1_TX_st {
    uint16_t command;
    int16_t torque;
    uint8_t mode;
  } PDO1_TX_st;

  uint8_t pdo_data[8];
};

union PDO1_RX_un {
  struct __attribute__((packed)) PDO1_RX_st {
    uint16_t status;
    int16_t torque;
    uint8_t mode;
  } PDO1_RX_st;

  uint8_t pdo_data[8];
};

union PDO2_TX_un {
  struct __attribute__((packed)) PDO2_TX_st {
    uint16_t command;
    int32_t position;
    uint8_t mode;
  } PDO2_TX_st;

  uint8_t pdo_data[8];
};
union PDO2_RX_un {
  struct __attribute__((packed)) PDO2_RX_st {
    uint16_t status;
    int32_t position;
    uint8_t mode;
  } PDO2_RX_st;

  uint8_t pdo_data[8];
};

union PDO3_TX_un {
  struct __attribute__((packed)) PDO3_TX_st {
    uint16_t command;
    int32_t speed;
    uint8_t mode;
  } PDO3_TX_st;

  uint8_t pdo_data[8];
};

union PDO3_RX_un {
  struct __attribute__((packed)) PDO3_RX_st {
    uint16_t status;
    int32_t speed;
    uint8_t mode;
  } PDO3_RX_st;

  uint8_t pdo_data[8];
};
union PDO4_TX_un {
  struct __attribute__((packed)) PDO4_TX_st {
    uint16_t torqueLimitPercent;
    uint8_t res1;
    uint8_t res2;
    uint8_t res3;
    uint8_t res4;
    uint8_t res5;
    uint8_t res6;
  } PDO4_TX_st;

  uint8_t pdo_data[8];
};
union SDO_un {
  struct __attribute__((packed)) SDO_st {
    uint8_t code;
    int16_t index;
    uint8_t subindex;
    int32_t data;
  } SDO_st;

  uint8_t sdo_data[8];
};

}  // namespace ELD2_CAN_data
#endif