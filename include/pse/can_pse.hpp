#ifndef CAN_PSE_H
#define CAN_PSE_H

#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
// #include <string.h>
// #include <sys/ioctl.h>
// #include <sys/socket.h>
// #include <sys/types.h>
#include <unistd.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <iostream>

#include "pse/pse.hpp"

enum can_baud_rates_t {
  CAN_BAUDRATE_20_KBPS = 20U,
  CAN_BAUDRATE_50_KBPS = 50U,
  CAN_BAUDRATE_100_KBPS = 100U,
  CAN_BAUDRATE_125_KBPS = 125U,
  CAN_BAUDRATE_250_KBPS = 250U,
  CAN_BAUDRATE_500_KBPS = 500U,
  CAN_BAUDRATE_800_KBPS = 800U,
  CAN_BAUDRATE_1_MBPS = 1000U,
  CAN_BAUDRATE_2_MBPS = 2000U,
};

class PseCan {
 public:
  int can_close();
  int can_open();
  int can_send(uint32_t id, uint8_t length, uint8_t data[8]);
  int can_recv();
  PseCan();
  virtual ~PseCan();
  bool sendExtendedId;
  using canRXCallback = std::function<void(struct can_frame *)>;
  using canTXCallback = std::function<void(void)>;
  void bindCanTXCallback(canTXCallback cb);
  void bindCanRXCallback(canRXCallback cb);

  int initInterface();
  void processCanTxEvent(const can_frame *msg);

  int initialized;

  uint16_t getErrorTx(void);
  uint16_t getErrorRx(void);

  void setBaudrate(uint16_t br);

 private:
  int fd;
  uint8_t candev;
  uint16_t baudrate;
  void canReadSome();
  void canRxHandler();
  boost::mutex mtx;
  // boost::asio::io_service ioService;
  // boost::asio::posix::stream_descriptor descriptor;

  uint16_t rxCallBackEna;
  canRXCallback rxCallBack;
  uint16_t txCallBackEna;
  canTXCallback txCallBack;

  uint16_t errorTx;
  uint16_t errorRx;

  can_frame canRxMsg;
  can_frame rx;
};

#endif