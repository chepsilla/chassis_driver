# Chassis Driver ROS2

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#docker">Docker</a></li>
      </ul>
    </li>
    <li>
      <a href="#ros2-api">ROS2 API</a>
      <ul>
        <li><a href="#subscribers">Subscribers</a></li>
        <li><a href="#publishers">Publishers</a></li>
      </ul>
    </li>
    <li><a href="#parameters">Parameters</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#useful links">Useful links</a></li>
  </ol>
</details>

## About The Project

This repository is a ROS2 driver for ASBIS Warehouse Robot chassis.

## Getting Started

### Prerequisites
* [CMake](https://cmake.org/install/)
* C++17
* [colcon](https://colcon.readthedocs.io/en/released/user/quick-start.html)
  ```
  sudo apt install python3-colcon-common-extensions
  ```
* [ROS2 Humble](https://docs.ros.org/en/humble/Installation.html)
* [ROS2 chassis_msgs](https://gitlab.com/autonomics.tech/warehouse/components/chassis_msgs)


### Installation

Create a workspace and clone repositories:
```bash
mkdir -p colcon_ws/src
cd colcon_ws/src
git clone --recursive https://gitlab.com/autonomics.tech/warehouse/components/chassis_driver.git --branch main
git clone https://gitlab.com/autonomics.tech/warehouse/components/chassis_msgs.git --branch main
```
Build the workspace:
```bash
cd ..
source /opt/ros/humble/setup.sh
colcon build --symlink-install
```

### Docker

Also it is possible to install and run driver with docker.

Build docker image:
```bash
cd chassis_driver
./docker/build.sh -i
```

Build driver inside docker environment:
```bash
./docker/build.sh -d
```

## ROS2 API

The driver represents a ROS2 node, subscribing to and publishing ROS2 topics.

### Subscribers
| Topic | Description |
|---|---|
| `/chassis/command` ([chassis_msgs/msg/ChassisCmd](https://gitlab.com/autonomics.tech/warehouse/components/chassis_msgs/-/blob/main/msg/ChassisCmd.msg)) | Chassis command from chassis converter node. |


### Publishers
| Topic | Description |
|---|---|
| `/chassis/feedback` ([chassis_msgs/msg/ChassisFeedback](https://gitlab.com/autonomics.tech/warehouse/components/chassis_msgs/-/blob/main/msg/ChassisFeedback.msg)) | Feedback from chassis. |

## Parameters

Parameters are configured in the [chassis.yml](config/chassis.yml) file.

| Parameter | Default | Units | Description |
|---|---|---|---|
| can_iface | "can0" | --- | Network interface name if chassis connected via IXXAT usb2can module. |
| frequency | 100.0 | Hz | Timer callback rate. All CAN data is read and sent in the callback of this timer. |
| devices.ids.front_left | 0x4 | --- | Front left motor CAN ID. |
| devices.ids.front_right | 0x3 | --- | Front right motor CAN ID. |
| devices.ids.rear_left | 0x2 | --- | Rear left motor CAN ID. |
| devices.ids.rear_right | 0x1 | --- | Rear right motor CAN ID. |
| settings.max_acceleration | 1000 | RPM^2 | Maximal acceleration value for all motors. |
| settings.max_deacceleration | 1000 | RPM^2 | Maximal acceleration value for all motors. |
| settings.max_motor_speed | 3000 | RPM | Maximal speed value for all motors |
| settings.torque_limit | 1000 | 0.1 % | Torque limit value for all motors. |


## Usage

Source ROS2 and the workspace:
```bash
source /opt/ros/humble/setup.sh
source colcon_ws/install/setup.sh --extend;
```
Launch ROS2 chassis driver node:
```bash
# xml launch
ros2 launch chassis_driver chassis.launch.xml

# python launch
ros2 launch chassis_driver chassis.launch.py
```

## Useful links

* 2ELD2-CAN7030B driver [user manual](https://www.leadshine.com/upfiles/downloads/11631b1e12586c32a34a1a16db971b54_1690179942715.pdf)
* ELVM80100V48EH-M17-HD motor [specification](https://www.leadshine.com/product-detail/ELVM80100V48EH-M17-HD.html)
