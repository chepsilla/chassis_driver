#!/bin/bash

yellow=`tput setaf 3`
green=`tput setaf 2`
violet=`tput setaf 5`
reset_color=`tput sgr0`

whitelist="-DCMAKE_BUILD_TYPE=Release --packages-up-to chassis_driver"
tag="latest"
arch=$(uname -m)

main () {
    cd "$(dirname "$0")";
    case "$1" in
        -i) image_build;;
        -d) debug_build;;
        -r) release_build;;
        *) echo ${yellow}"$1 is not a correct key. Choose '-i' for image build, '-d' for debug build, '-r' for release build"${reset_color};;
    esac
}

image_build () {
    echo ${green}"Building image from Dockerfile: "${reset_color};    
    docker build -f ./Dockerfile . -t ${arch}humble/chassis_driver:latest;
}

start_container () {
    if [ ! "$(docker ps -q -f name=chassis_driver)" ]; then
        cd ..; cd ..;
        workspace_dir=$PWD;  
        echo ${green}"Starting the chassis_driver container, tag: ${violet}$tag"${reset_color};
        cd "$(dirname "$0")";
        docker run -it -d --rm \
            --privileged \
            --name chassis_driver \
            --net "host" \
            -v $workspace_dir/:/home/docker_chassis_driver/colcon_ws/src/:rw \
            ${arch}humble/chassis_driver:$tag
    fi
}

debug_build () {
    start_container &&
    echo ${green}"Entering the chassis_driver container and building in DEBUG mode: "${reset_color};
    docker exec --user "docker_chassis_driver" -it chassis_driver \
    /bin/bash -c "source /opt/ros/humble/setup.sh; cd /home/docker_chassis_driver/colcon_ws; colcon build --cmake-args ${whitelist}; /bin/bash";
}

release_build () {
    tag="release"
    start_container &&
    echo ${green}"Entering the reaper_sweeper container and building in RELEASE mode: "${reset_color};
    docker exec --user "docker_reaper_sweeper" -it reaper_sweeper \
    /bin/bash -c "source /opt/ros/humble/setup.sh; cd /home/docker_reaper_sweeper/colcon_ws; colcon build --cmake-args ${whitelist} &&\
    rm -rf build/ devel/" && echo ${green}"Committing the release of reaper_sweeper container: "${reset_color};
}

main "$@"; exit