#!/bin/bash

cd "$(dirname "$0")";
cd ..
cd ..
workspace_dir=$PWD

if [ "$(docker ps -aq -f status=exited -f name=chassis_driver)" ]; then
    docker rm chassis_driver;
fi

tag="latest"
if [ "$#" -ne  "0" ]; then
    case "$1" in
        -l) ;;
        -r) tag="release";;
        *) echo "$1 is not a correct key. Choose '-l' for 'latest' tag and '-r' for 'release' tag";;
    esac
fi

if [[ $tag == "latest" ]]; then
    docker run -it -d --rm \
        --name chassis_driver \
        --net host \
        -v $workspace_dir/:/home/docker_chassis_driver/colcon_ws/src/:rw \
        $(uname -m)humble/chassis_driver:$tag
else
    docker run -it -d --rm \
        --name chassis_driver \
        --net host \
        $(uname -m)humble/chassis_driver:$tag
fi