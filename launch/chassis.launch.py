import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()
    config = os.path.join(
        get_package_share_directory("chassis_driver"), "config", "chassis.yml"
    )

    node = Node(
        package="chassis_driver",
        name="chassis_driver_node",
        executable="chassis_driver_node",
        parameters=[config],
    )
    ld.add_action(node)
    return ld
