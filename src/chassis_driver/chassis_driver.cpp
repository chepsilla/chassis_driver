#include "chassis_driver/chassis_driver.hpp"

ChassisDriver::ChassisDriver() : rclcpp::Node("chassis_driver_node") {
  readParameters();

  initializeCANDevices();

  auto frequency = declareAndGetParameter<int>("frequency");
  auto queueSize = frequency;

  timerCbGroup =
      create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  subCbGroup =
      create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);

  chassisFeedback.rtp_type = ChassisFeedback::MECANUM_CHASSIS;
  chassisFeedback.rtp_electric_motor = std::vector<MotorData>(4);
  timer = create_wall_timer(std::chrono::milliseconds(1000 / frequency),
                            std::bind(&ChassisDriver::timerCallback, this),
                            timerCbGroup);
  rclcpp::SubscriptionOptions subCbOptions;
  subCbOptions.callback_group = subCbGroup;

  rmw_qos_profile_t qosProfile = rmw_qos_profile_sensor_data;
  auto qos = rclcpp::QoS(
      rclcpp::QoSInitialization(qosProfile.history, queueSize), qosProfile);

  commandSub = create_subscription<ChassisCmd>(
      "/chassis/command", qos,
      std::bind(&ChassisDriver::commandCallback, this, std::placeholders::_1),
      subCbOptions);
  feedbackPub = create_publisher<ChassisFeedback>("/chassis/feedback", qos);
}

ChassisDriver::~ChassisDriver() {}

template <class T>
inline T ChassisDriver::declareAndGetParameter(std::string name) {
  if (!has_parameter(name)) declare_parameter<T>(name);
  T ret_value{};
  get_parameter(name, ret_value);
  RCLCPP_INFO_STREAM(get_logger(), name << ": " << ret_value);
  return ret_value;
}

void ChassisDriver::readParameters() {
  use_pse = declareAndGetParameter<bool>("use_pse");
  can_iface = declareAndGetParameter<std::string>("can_iface");
  flId = static_cast<uint16_t>(
      declareAndGetParameter<int>("devices.ids.front_left"));
  frId = static_cast<uint16_t>(
      declareAndGetParameter<int>("devices.ids.front_right"));
  rlId = static_cast<uint16_t>(
      declareAndGetParameter<int>("devices.ids.rear_left"));
  rrId = static_cast<uint16_t>(
      declareAndGetParameter<int>("devices.ids.rear_right"));
  driverSettings.maxAcceleration = static_cast<uint32_t>(
      declareAndGetParameter<int>("settings.max_acceleration"));
  driverSettings.maxDeacceleration = static_cast<uint32_t>(
      declareAndGetParameter<int>("settings.max_deacceleration"));
  driverSettings.maxMotorSpeed = static_cast<uint32_t>(
      declareAndGetParameter<int>("settings.max_motor_speed"));
  driverSettings.maxTorquePercent = static_cast<uint32_t>(
      declareAndGetParameter<int>("settings.torque_limit"));
}

void ChassisDriver::initializeCANDevices() {
  if (use_pse) {
    pseCan = std::make_shared<PseCan>();
    pseCan->sendExtendedId = false;
    pseCan->setBaudrate(1000);
    if (pseCan->initInterface() != 0) {
      RCLCPP_ERROR(this->get_logger(), "Could not open device: /dev/pse");
    } else {
      pseCan->initialized = true;

      flELD2 = std::make_shared<ELD2CAN>(flId, ELD2CAN::SYNC_ENABLED);
      flELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(flELD2->getNodeId(), flELD2.get()));
      flELD2->bindCanTXCallback(std::bind(&PseCan::processCanTxEvent,
                                          pseCan.get(), std::placeholders::_1),
                                pseCan->initialized);
      flELD2->setDirValue(1);

      frELD2 = std::make_shared<ELD2CAN>(frId, ELD2CAN::SYNC_DISABLED);
      frELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(frELD2->getNodeId(), frELD2.get()));
      frELD2->bindCanTXCallback(std::bind(&PseCan::processCanTxEvent,
                                          pseCan.get(), std::placeholders::_1),
                                pseCan->initialized);
      frELD2->setDirValue(0);

      rlELD2 = std::make_shared<ELD2CAN>(rlId, ELD2CAN::SYNC_DISABLED);
      rlELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(rlELD2->getNodeId(), rlELD2.get()));
      rlELD2->bindCanTXCallback(std::bind(&PseCan::processCanTxEvent,
                                          pseCan.get(), std::placeholders::_1),
                                pseCan->initialized);
      rlELD2->setDirValue(1);

      rrELD2 = std::make_shared<ELD2CAN>(rrId, ELD2CAN::SYNC_DISABLED);
      rrELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(rrELD2->getNodeId(), rrELD2.get()));
      rrELD2->bindCanTXCallback(std::bind(&PseCan::processCanTxEvent,
                                          pseCan.get(), std::placeholders::_1),
                                pseCan->initialized);
      rrELD2->setDirValue(0);

      pseCan->bindCanRXCallback(std::bind(&ChassisDriver::canRxCallback, this,
                                          std::placeholders::_1));
    }
  } else {
    socketCan = std::make_shared<SocketCan>();
    socketCan->sendExtendedId = false;
    if (socketCan->initInterface(can_iface) != 0) {
      RCLCPP_ERROR(this->get_logger(), "Could not open device: %s",
                   can_iface.c_str());
    } else {
      socketCan->initialized = true;

      flELD2 = std::make_shared<ELD2CAN>(flId, ELD2CAN::SYNC_ENABLED);
      flELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(flELD2->getNodeId(), flELD2.get()));
      flELD2->bindCanTXCallback(
          std::bind(&SocketCan::processCanTxEvent, socketCan.get(),
                    std::placeholders::_1),
          socketCan->initialized);
      flELD2->setDirValue(1);

      frELD2 = std::make_shared<ELD2CAN>(frId, ELD2CAN::SYNC_DISABLED);
      frELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(frELD2->getNodeId(), frELD2.get()));
      frELD2->bindCanTXCallback(
          std::bind(&SocketCan::processCanTxEvent, socketCan.get(),
                    std::placeholders::_1),
          socketCan->initialized);
      frELD2->setDirValue(0);

      rlELD2 = std::make_shared<ELD2CAN>(rlId, ELD2CAN::SYNC_DISABLED);
      rlELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(rlELD2->getNodeId(), rlELD2.get()));
      rlELD2->bindCanTXCallback(
          std::bind(&SocketCan::processCanTxEvent, socketCan.get(),
                    std::placeholders::_1),
          socketCan->initialized);
      rlELD2->setDirValue(1);

      rrELD2 = std::make_shared<ELD2CAN>(rrId, ELD2CAN::SYNC_DISABLED);
      rrELD2->settings = driverSettings;
      rxCallbackMap.insert(std::make_pair(rrELD2->getNodeId(), rrELD2.get()));
      rrELD2->bindCanTXCallback(
          std::bind(&SocketCan::processCanTxEvent, socketCan.get(),
                    std::placeholders::_1),
          socketCan->initialized);
      rrELD2->setDirValue(0);

      socketCan->bindCanRXCallback(std::bind(&ChassisDriver::canRxCallback,
                                             this, std::placeholders::_1));
    }
  }
}

void ChassisDriver::canRxCallback(struct can_frame *msg) {
  if (rxCallbackMap[msg->can_id & id_filter_mask]) {
    rxCallbackMap[msg->can_id & id_filter_mask]->rxParse(msg);
  }
}

void ChassisDriver::timerCallback() {
  chassisFeedback.moving_ready = false;
  if ((!use_pse && socketCan->initialized) ||
      (use_pse && pseCan->initialized)) {
    flELD2->deviceTimeTick();
    frELD2->deviceTimeTick();
    rrELD2->deviceTimeTick();
    rlELD2->deviceTimeTick();

    feedbackHelper(flELD2, 0);
    feedbackHelper(frELD2, 1);
    feedbackHelper(rrELD2, 2);
    feedbackHelper(rlELD2, 3);

    uint16_t actualError = 0;
    if (use_pse) {
      if ((actualError = pseCan->getErrorRx()) != 0) {
        RCLCPP_ERROR(this->get_logger(), "CAN RX error is : %d", actualError);
      }
      if ((actualError = pseCan->getErrorTx()) != 0) {
        RCLCPP_ERROR(this->get_logger(), "CAN TX error is : %d", actualError);
      }
    } else {
      if ((actualError = socketCan->getErrorRx()) != 0) {
        RCLCPP_ERROR(this->get_logger(), "CAN RX error is : %d", actualError);
      }
      if ((actualError = socketCan->getErrorTx()) != 0) {
        RCLCPP_ERROR(this->get_logger(), "CAN TX error is : %d", actualError);
      }
    }
    if ((flELD2->ELD2_data.errorCode == 0) &&
        (frELD2->ELD2_data.errorCode == 0) &&
        (rlELD2->ELD2_data.errorCode == 0) &&
        (rrELD2->ELD2_data.errorCode == 0)) {
      chassisFeedback.moving_ready = true;
    } else {
      RCLCPP_ERROR_THROTTLE(
          this->get_logger(), *this->get_clock(), 1000,
          "Front Left: %d; Front Right: %d; Rear Left %d, Rear Right: %d",
          flELD2->ELD2_data.errorCode, frELD2->ELD2_data.errorCode,
          rlELD2->ELD2_data.errorCode, rrELD2->ELD2_data.errorCode);
    }
  }
  chassisFeedback.header.stamp = rclcpp::Clock(RCL_ROS_TIME).now();
  feedbackPub->publish(chassisFeedback);
}

void ChassisDriver::feedbackHelper(std::shared_ptr<ELD2CAN> drive, int id) {
  chassisFeedback.rtp_electric_motor[id].speed = drive->ELD2_data.fbSpeed;
  chassisFeedback.rtp_electric_motor[id].torque = drive->ELD2_data.fbTorque;
  chassisFeedback.rtp_electric_motor[id].angle = drive->ELD2_data.fbPosition;
  chassisFeedback.rtp_electric_motor[id].status =
      drive->ELD2_data.status.statusWordSt.operationEnabled;
  switch (drive->ELD2_data.actualMode) {
    case ELD2CAN::LOOP_MODE_STATE::SPEED:
      chassisFeedback.rtp_electric_motor[id].current_mode = MotorData::RPM_MODE;
      break;
    case ELD2CAN::LOOP_MODE_STATE::TORQUE:
      chassisFeedback.rtp_electric_motor[id].current_mode =
          MotorData::TORQUE_MODE;
      break;
    case ELD2CAN::LOOP_MODE_STATE::POSITION:
      chassisFeedback.rtp_electric_motor[id].current_mode =
          MotorData::POSE_MODE;
      break;
    default:
      break;
  }
}

void ChassisDriver::commandTypeHelper(const MotorCmd &cmd,
                                      std::shared_ptr<ELD2CAN> drive) {
  switch (cmd.cmd_type) {
    case MotorCmd::POSE_CMD:
      drive->setLoopValue(MotorCmd::POSE_CMD);
      drive->setRefValue(cmd.angle);
      break;
    case MotorCmd::RPM_CMD:
      drive->setLoopValue(MotorCmd::RPM_CMD);
      drive->setRefValue(cmd.speed);
      break;
    case MotorCmd::TORQUE_CMD:
      drive->setLoopValue(MotorCmd::TORQUE_CMD);
      drive->setRefValue(cmd.torque);
      break;
    default:
      break;
  }
  if (cmd.start_flag) {
    drive->setCmdValue(1);
  } else
    drive->setCmdValue(0);
}

void ChassisDriver::commandCallback(const ChassisCmd::SharedPtr msg) {
  commandTypeHelper(msg->motor_cmd[0], flELD2);
  commandTypeHelper(msg->motor_cmd[1], frELD2);
  commandTypeHelper(msg->motor_cmd[2], rrELD2);
  commandTypeHelper(msg->motor_cmd[3], rlELD2);
}