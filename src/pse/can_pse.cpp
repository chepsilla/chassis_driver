#include "pse/can_pse.hpp"

/// Disable the can device
int PseCan::can_close() {
  can_command_t command = {.op = kCAN_Disable, .dev = candev};

  if (pse_command_checked(fd, kHECI_CAN_COMMAND, *(uint16_t *)&command, NULL,
                          NULL) < 0) {
    std::cerr << "Failed to close the CAN device\n";
    return -1;
  }

  return 0;
}

int PseCan::can_open() {
  can_command_t command = {
      .op = kCAN_SetBaudrate,
      .dev = candev,
      .arg = baudrate,
  };

  fd = pse_client_connect();

  if (fd <= 0) {
    std::cerr << "Failed to establish a connection with the PSE\n";
    return -1;
  }

  // Set the baudrate
  if (pse_command_checked(fd, kHECI_CAN_COMMAND, *(uint16_t *)&command, NULL,
                          NULL) < 0) {
    std::cerr << "Could not set the CAN baudrate " << (int)command.arg << "\n";
    return -1;
  }

  // Open the CAN device
  command.op = kCAN_Enable;
  if (pse_command_checked(fd, kHECI_CAN_COMMAND, *(uint16_t *)&command, NULL,
                          NULL) < 0) {
    std::cerr << "Could not open the CAN device\n";
    return -1;
  }

  return 0;
}

/// Send a can (data) frame
int PseCan::can_send(uint32_t id, uint8_t length, uint8_t data[8]) {
  can_command_t command = {
      .op = kCAN_Write,
      .dev = candev,
  };

  heci_body_t body = {
      .kind = kHeciData_Can,
      .length = sizeof(heci_can_data_t),
  };

  heci_can_data_t *frame = (heci_can_data_t *)(body.data);

  // Sanity checks
  if (length > 8) {
    std::cerr << "The maximum CAN frame length is 8 bytes\n";
    return -1;
  }

  if (id > 0x1FFFFFFF) {
    std::cerr << "The message ID exceded the maximum CAN frame ID\n";
    return -1;
  }

  // Build the can frame
  frame->id = id;
  frame->id_type = id < 0x7FF ? 0 : 1;
  frame->frame_type = 0;
  frame->length = length;

  frame->data_word_0 =
      data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
  frame->data_word_1 =
      data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 24);

  // Send the CAN frame
  if (pse_command_checked(fd, kHECI_CAN_COMMAND, *(uint16_t *)&command, &body,
                          NULL) < 0) {
    std::cerr << "Failed to send the CAN frame\n";
    return -1;
  }

  return 0;
}

/// Receive a CAN (data) frame
int PseCan::can_recv() {
  heci_body_t body;
  heci_can_data_t *frame;
  can_frame res{};

  can_command_t command = {.op = kCAN_Read, .dev = candev};

  mtx.lock();
  int ret = pse_command_checked(fd, kHECI_CAN_COMMAND, *(uint16_t *)&command,
                                NULL, &body);
  mtx.unlock();

  if (ret < 0) {
    std::cerr << "Failed to request a CAN frame from the PSE (" << ret << ")\n";
    return -1;
  } else if (ret != 1) {
    std::cerr << "Read a CAN frame from the PSE, but did not receive any data\n";
    return -1;
  }

  frame = (heci_can_data_t *)body.data;

  res.can_id = frame->id;
  res.len = frame->length;

  for (uint8_t i = 0; i < frame->length; i++) {
    if (i < 4) {
      res.data[i] = *((uint8_t *)&frame->data_word_0 + i);
    } else {
      res.data[i] = *((uint8_t *)&frame->data_word_1 + i - 4);
    }
  }

  // for debug
  std::cerr << "recv: " << frame->id<< " ," << frame->length << ", " << res.data << "\n";

  rx = res;
  return 0;
}

PseCan::PseCan()
    : initialized(false),
      candev(0),
      rxCallBackEna(false),
      rxCallBack(nullptr),
      txCallBackEna(false),
      txCallBack(nullptr),
      errorTx(0),
      errorRx(0) {}

PseCan::~PseCan() { can_close(); }

void PseCan::bindCanTXCallback(canTXCallback cb) {
  if (cb != NULL) txCallBack = cb;
  txCallBackEna = true;
}

void PseCan::bindCanRXCallback(canRXCallback cb) {
  if (cb != NULL) rxCallBack = cb;
  rxCallBackEna = true;
}

int PseCan::initInterface() {
  int ret = can_open();

  boost::thread t(boost::bind(&PseCan::canReadSome, this));
  t.detach();
  return ret;
}

void PseCan::processCanTxEvent(const struct can_frame *msg) {
  std::cout << __FUNCTION__ << ": " << boost::this_thread::get_id() << "\n";
  can_frame tx = {};
  if (msg->can_id > 0x7FF) {
    tx.can_id = (msg->can_id & 0x1FFFFFFF) | CAN_EFF_FLAG;
  } else {
    tx.can_id = (msg->can_id & 0x7ff);
  }

  tx.len = msg->len;

  for (int i = 0; i < msg->len; i++) {
    tx.data[i] = msg->data[i];
  }

  // boost::thread t(
  //     boost::bind(&PseCan::can_send, this, tx.can_id, tx.len, tx.data));
  // t.detach();

  // mutex lock?
  mtx.lock();
  can_send(tx.can_id, tx.len, tx.data);
  mtx.unlock();

  if (txCallBackEna) {
    txCallBack();
  }
}

uint16_t PseCan::getErrorTx(void) { return (errorTx); }
uint16_t PseCan::getErrorRx(void) { return (errorRx); }
void PseCan::setBaudrate(uint16_t br) { baudrate = br; }

void PseCan::canRxHandler() {
  std::cout << __FUNCTION__ << ": " << boost::this_thread::get_id() << "\n";
  if (initialized) {
    canRxMsg.can_id = rx.can_id & 0x1FFFFFFF;
    canRxMsg.len = rx.len;
    for (int i = 0; i < rx.len; i++) {
      canRxMsg.data[i] = rx.data[i];
    }
    if (rxCallBackEna) {
      rxCallBack(&canRxMsg);
    }
  }
}

void PseCan::canReadSome() {
  std::cout << __FUNCTION__ << ": " << boost::this_thread::get_id() << "\n";
  if (can_recv() == 0) {
    canRxHandler();
  }
  canReadSome();
}