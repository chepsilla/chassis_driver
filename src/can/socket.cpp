#include "can/socket.hpp"

SocketCan::SocketCan()
    : initialized(false),
      descriptor(ioService),
      rxCallBackEna(false),
      rxCallBack(nullptr),
      txCallBackEna(false),
      txCallBack(nullptr),
      errorTx(0),
      errorRx(0) {}

SocketCan::~SocketCan() { close(s); }

void SocketCan::bindCanTXCallback(canTXCallback cb) {
  if (cb != NULL) txCallBack = cb;
  txCallBackEna = true;
}

void SocketCan::bindCanRXCallback(canRXCallback cb) {
  if (cb != NULL) rxCallBack = cb;
  rxCallBackEna = true;
}

int SocketCan::initInterface(std::string interface) {
  struct sockaddr_can addr = {};
  struct ifreq ifr = {};
  int ret;

  // open socket
  if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
    std::cerr << "Cannot create CAN socket err: " << s << std::endl;
    return -1;
  }

  addr.can_family = AF_CAN;

  strcpy(ifr.ifr_name, interface.c_str());
  if ((ret = ioctl(s, SIOCGIFINDEX, &ifr)) < 0) {
    std::cerr << "Error setting interface name: " << ret << std::endl;
    return -1;
  }

  addr.can_ifindex = ifr.ifr_ifindex;

  if ((ret = bind(s, (struct sockaddr *)&addr, sizeof(addr))) < 0) {
    std::cerr << "Error Binding to socket: " << ret << std::endl;
    return -1;
  }

  descriptor.assign(s);

  canReadSome();

  boost::thread t(boost::bind(&boost::asio::io_service::run, &ioService));
  t.detach();
  return 0;
}

void SocketCan::processCanTxEvent(const struct can_frame *msg) {
  can_frame tx = {};
  if (msg->can_id > 0x7FF) {
    tx.can_id = (msg->can_id & 0x1FFFFFFF) | CAN_EFF_FLAG;
  } else {
    tx.can_id = (msg->can_id & 0x7ff);
  }

  tx.len = msg->len;

  for (int i = 0; i < msg->len; i++) {
    tx.data[i] = msg->data[i];
  }
  try {
    boost::asio::write(descriptor, boost::asio::buffer(&tx, sizeof(tx)));
  } catch (boost::system::system_error &e) {
    std::cerr << e.what() << std::endl;
    errorTx = e.code().value();
  }

  if (txCallBackEna) {
    txCallBack();
  }
}

uint16_t SocketCan::getErrorTx(void) { return (errorTx); }
uint16_t SocketCan::getErrorRx(void) { return (errorRx); }

void SocketCan::canRxHandler(const boost::system::error_code &error,
                             size_t bytes_transferred) {
  if (bytes_transferred && initialized) {
    canRxMsg.can_id = rx.can_id & 0x1FFFFFFF;
    canRxMsg.len = rx.len;
    for (int i = 0; i < rx.len; i++) {
      canRxMsg.data[i] = rx.data[i];
    }
    if (rxCallBackEna) {
      rxCallBack(&canRxMsg);
    }
  }
  errorRx = error.value();
  canReadSome();
}

void SocketCan::canReadSome() {
  descriptor.async_read_some(
      boost::asio::buffer(&rx, sizeof(rx)),
      boost::bind(&SocketCan::canRxHandler, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}
