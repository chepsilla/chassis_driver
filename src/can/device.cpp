#include "can/device.hpp"

CANDevice::CANDevice(uint16_t nodeid, SYNCSTATE state) {
  localNodeId = nodeid;
  masterState = state;
  sendToCAN = nullptr;
  deviceOnline = 0;
}

CANDevice::~CANDevice() {}

uint16_t CANDevice::getNodeId(void) { return (localNodeId); }

void CANDevice::bindCanTXCallback(canTXCallback cb, bool CAN_Enabled) {
  if ((cb != NULL) && CAN_Enabled) sendToCAN = cb;
}

// sdo Helper function
void CANDevice::sendSDO(uint8_t cmd, uint16_t index, uint8_t sub_idx,
                        uint32_t data) {
  std::memset(&canTx, 0, sizeof(canTx));
  canTx.can_id = 0x600 + localNodeId;
  canTx.can_dlc = 8;
  canTx.data[0] = cmd;                  // command
  canTx.data[1] = (index & 0xFF);       // LSB of parameter address
  canTx.data[2] = (index >> 8) & 0xFF;  // MSB of parameter addres
  canTx.data[3] = sub_idx;
  // Data field
  canTx.data[4] = (data >> 0) & 0xFF;
  canTx.data[5] = (data >> 8) & 0xFF;
  canTx.data[6] = (data >> 16) & 0xFF;
  canTx.data[7] = (data >> 24) & 0xFF;
  sendToCAN(&canTx);
}

// Send PDO helper function
void CANDevice::sendPDO(uint16_t pdo_num, uint8_t *data) {
  std::memset(&canTx, 0, sizeof(canTx));
  canTx.can_id = pdo_num + localNodeId;
  canTx.can_dlc = 8;
  // control word field
  std::memcpy(&canTx.data[0], data, 8);
  sendToCAN(&canTx);
}

// Send NMT helper function
void CANDevice::sendNMT(uint8_t cmd) {
  std::memset(&canTx, 0, sizeof(canTx));
  // make NMT command with nodeid
  canTx.can_id = 0;             // NMT broadcast ID
  canTx.can_dlc = 2;            // NMT data
  canTx.data[0] = cmd;          // NMT Operation command
  canTx.data[1] = localNodeId;  // Node ID
  sendToCAN(&canTx);
}

// Send SYNC helper function
void CANDevice::sendSYNC(void) {
  std::memset(&canTx, 0, sizeof(canTx));
  // make NMT command with nodeid
  canTx.can_id = 0x80;  // SYNC broadcast ID
  canTx.can_dlc = 0;    // SYNC data
  // Send SYNC message only if node is a master
  if (masterState == SYNC_ENABLED) sendToCAN(&canTx);
}

// Send raw message
void CANDevice::sendRAW(can_frame &mes) { sendToCAN(&mes); }