#include "devices/eld2_can.hpp"

ELD2CAN::ELD2CAN(uint16_t nodeid, SYNCSTATE state) : CANDevice(nodeid, state) {
  nodeStatus = NODE_NOTINIT;
  initState = MAKE_NMT_OPERATIONAL;
  controlState = DISABLE;
  ELD2_data.errorCode = 0xAA;
  settings.maxMotorSpeed = MAX_MOTOR_SPEED_PULSE;
  settings.maxAcceleration = MAX_MOTOR_ACCEL;
  settings.maxDeacceleration = MAX_MOTOR_ACCEL;
  commandFlashBit = 0;
  cmdWaitCounter = CMDWAITCOUNTER;
  localLoop = POSITION;
  localRef = 0;
  localCmd = 0;
  localDir = 0;

  PDO1_tx = {};
  PDO2_tx = {};
  PDO3_tx = {};
  PDO4_tx = {};
  PDO1_rx = {};
  PDO2_rx = {};
  PDO3_rx = {};
  SDO_tx = {};
  SDO_rx = {};
}

ELD2CAN::~ELD2CAN() {}

void ELD2CAN::deviceInit(void) {
  switch (initState) {
    case MAKE_NMT_OPERATIONAL:
      // make NMT command with nodeid
      sendNMT(0x1);
      if (deviceOnline) {
        initState = SET_CONTROL_MODE_6060;
      }
      break;
    case SET_CONTROL_MODE_6060: {
      sendSDO(SDO_2F, 0x6060, 0, this->getLoopValue());
      initState = SET_SPEED_LIMIT_607F;
      break;
    }
    case SET_SPEED_LIMIT_607F: {
      uint32_t motorMaxSpeed =
          (settings.maxMotorSpeed * MOTOR_SPEED_SCALE < MAX_MOTOR_SPEED_PULSE)
              ? settings.maxMotorSpeed * MOTOR_SPEED_SCALE
              : MAX_MOTOR_SPEED_PULSE;
      sendSDO(SDO_23, 0x607f, 0, motorMaxSpeed);
      initState = SET_ACCEL_LIMIT_6083;
      break;
    }
    case SET_ACCEL_LIMIT_6083: {
      uint32_t motorMaxAceel =
          (settings.maxAcceleration * MOTOR_SPEED_SCALE < MAX_MOTOR_ACCEL)
              ? settings.maxAcceleration * MOTOR_SPEED_SCALE
              : MAX_MOTOR_ACCEL;
      sendSDO(SDO_23, 0x6083, 0, motorMaxAceel);
      initState = SET_ACCEL_LIMIT_6084;
      break;
    }
    case SET_ACCEL_LIMIT_6084: {
      uint32_t motorMaxAceel =
          (settings.maxDeacceleration * MOTOR_SPEED_SCALE < MAX_MOTOR_ACCEL)
              ? settings.maxDeacceleration * MOTOR_SPEED_SCALE
              : MAX_MOTOR_ACCEL;
      sendSDO(SDO_23, 0x6084, 0, motorMaxAceel);
      initState = SET_ACCEL_LIMIT_6085;
      break;
    }
    case SET_ACCEL_LIMIT_6085: {
      uint32_t motorMaxAceel =
          (settings.maxDeacceleration * MOTOR_SPEED_SCALE < MAX_MOTOR_ACCEL)
              ? settings.maxDeacceleration * MOTOR_SPEED_SCALE
              : MAX_MOTOR_ACCEL;
      sendSDO(SDO_23, 0x6085, 0, motorMaxAceel);
      initState = SET_PULSES_PER_REVOLUTION_2008;
      break;
    }
    case SET_PULSES_PER_REVOLUTION_2008: {
      sendSDO(SDO_23, 0x2008, 0, ENCODER_PULSE);
      initState = SET_HOMEBACK_MODE_6098;
      break;
    }
    case SET_HOMEBACK_MODE_6098: {
      sendSDO(SDO_2F, 0x6098, 0, 0);
      initState = SET_HOMEBACK_ACCEL_609A;
      break;
    }
    case SET_HOMEBACK_ACCEL_609A: {
      sendSDO(SDO_23, 0x609A, 0, 100);
      initState = SET_HOMEBACK_SPEED_6099_1;
      break;
    }
    case SET_HOMEBACK_SPEED_6099_1: {
      sendSDO(SDO_23, 0x6099, 1, 100);
      initState = SET_HOMEBACK_SPEED_6099_2;
      break;
    }
    case SET_HOMEBACK_SPEED_6099_2: {
      sendSDO(SDO_23, 0x6099, 2, 100);
      initState = SET_HOMEBACK_OFFSET_607C;
      break;
    }
    case SET_HOMEBACK_OFFSET_607C: {
      sendSDO(SDO_23, 0x607C, 0, 0);
      initState = MAX_ENUM;
      break;
    }
    case MAX_ENUM: {
      nodeStatus = NODE_INITIALIZED;
      break;
    }
    default:
      break;
  }
}

NODE_STATE ELD2CAN::getState(void) { return (nodeStatus); }

void ELD2CAN::setSpeed(uint32_t ref) { sendSDO(SDO_23, 0x6081, 0, ref); }

void ELD2CAN::setLoopMode(uint8_t mode) { sendSDO(SDO_2F, 0x6060, 0, mode); }

//  set control function
void ELD2CAN::deviceCtrl(void) {
  int8_t refSign = (this->getDirValue() > 0) ? 1 : -1;
  if (cmdWaitCounter != CMDWAITINFINITY) {
    if (commandFlashBit == false) {
      if (cmdWaitCounter-- < 1) {
        this->setCmdValue(0);
        this->setRefValue(0);
        cmdWaitCounter = CMDWAITCOUNTER;
      }
    } else {
      cmdWaitCounter = CMDWAITCOUNTER;
    }
  }
  // clear command bit
  commandFlashBit = false;
  if ((nodeStatus == NODE_INITIALIZED) || (nodeStatus == NODE_ACTIVE)) {
    if (this->getCmdValue() == 0) controlState = DISABLE;

    //  Command word state machine
    switch (controlState) {
      case DISABLE: {
        PDO1_tx.PDO1_TX_st.command = 0x6;
        if (this->getLoopValue() == TORQUE) {
          PDO1_tx.PDO1_TX_st.torque = 0;
          PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
          makePDO1();
        }
        if (this->getLoopValue() == POSITION) {
          PDO2_tx.PDO2_TX_st.position = 0;
          PDO2_tx.PDO2_TX_st.mode = this->getLoopValue();
          makePDO2();
        }
        if (this->getLoopValue() == SPEED) {
          PDO3_tx.PDO3_TX_st.speed = 0;
          PDO3_tx.PDO3_TX_st.mode = this->getLoopValue();
          makePDO3();
        }
        controlState = CLEAR_ERROR;
        break;
      }
      case CLEAR_ERROR: {
        PDO1_tx.PDO1_TX_st.command = 0xF0;
        PDO1_tx.PDO1_TX_st.torque = 0x0;
        PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
        makePDO1();
        controlState = GOTO_READY;
        break;
      }
      case GOTO_READY: {
        PDO1_tx.PDO1_TX_st.command = 0x6;
        PDO1_tx.PDO1_TX_st.torque = 0x0;
        PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
        makePDO1();
        controlState = GOTO_ENABLED;
        break;
      }
      case GOTO_ENABLED: {
        PDO1_tx.PDO1_TX_st.command = 0x7;
        PDO1_tx.PDO1_TX_st.torque = 0x0;
        PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
        makePDO1();
        controlState = GOTO_ACTIVE;
        break;
      }
      case GOTO_ACTIVE: {
        if (this->getLoopValue() == TORQUE) {
          PDO1_tx.PDO1_TX_st.command = 0xF;
          PDO1_tx.PDO1_TX_st.torque = this->getRefValue() * refSign;
          PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
          makePDO1();
        }
        if (this->getLoopValue() == POSITION) {
          PDO2_tx.PDO2_TX_st.command = 0x2F;
          PDO2_tx.PDO2_TX_st.position = this->getRefValue();
          PDO2_tx.PDO2_TX_st.mode = this->getLoopValue();
          makePDO2();
        }
        if (this->getLoopValue() == SPEED) {
          PDO3_tx.PDO3_TX_st.command = 0xF;
          int32_t speedRef = this->getRefValue() * MOTOR_SPEED_SCALE;
          PDO3_tx.PDO3_TX_st.speed = static_cast<int32_t>(speedRef * refSign);
          PDO3_tx.PDO3_TX_st.mode = this->getLoopValue();
          makePDO3();
        }

        nodeStatus = NODE_ACTIVE;
        controlState = GOTO_COMMAMD;
        break;
      }
      case GOTO_COMMAMD: {
        if (this->getLoopValue() == TORQUE) {
          PDO1_tx.PDO1_TX_st.command = 0xF;
          PDO1_tx.PDO1_TX_st.torque =
              static_cast<int16_t>(this->getRefValue() * refSign);
          PDO1_tx.PDO1_TX_st.mode = this->getLoopValue();
          makePDO1();
        }
        if (this->getLoopValue() == POSITION) {
          PDO2_tx.PDO2_TX_st.command = 0x3F;
          PDO2_tx.PDO2_TX_st.position =
              static_cast<int32_t>(this->getRefValue() * MOTOR_POSITION_SCALE);
          PDO2_tx.PDO2_TX_st.mode = this->getLoopValue();
          makePDO2();
        }
        if (this->getLoopValue() == SPEED) {
          PDO3_tx.PDO3_TX_st.command = 0xF;
          int32_t speedRef = this->getRefValue() * MOTOR_SPEED_SCALE;
          PDO3_tx.PDO3_TX_st.speed = static_cast<int32_t>(speedRef * refSign);
          PDO3_tx.PDO3_TX_st.mode = this->getLoopValue();
          makePDO3();
          PDO4_tx.PDO4_TX_st.torqueLimitPercent = settings.maxTorquePercent;
          makePDO4();
        }
        controlState = GOTO_ACTIVE;
        break;
      }
      default:
        break;
    }
  }
  // Update data variable
  ELD2_data.control.controlWord = PDO1_tx.PDO1_TX_st.command;
  ELD2_data.status.statusWord = PDO1_rx.PDO1_RX_st.status;
  ELD2_data.refTorque = PDO1_tx.PDO1_TX_st.torque;
  ELD2_data.fbTorque = PDO1_rx.PDO1_RX_st.torque;
  ELD2_data.refPosition = PDO2_tx.PDO2_TX_st.position / MOTOR_POSITION_SCALE;
  ELD2_data.fbPosition = PDO2_rx.PDO2_RX_st.position / MOTOR_POSITION_SCALE;
  ELD2_data.refSpeed = PDO3_tx.PDO3_TX_st.speed / MOTOR_SPEED_SCALE * refSign;
  ELD2_data.fbSpeed = PDO3_rx.PDO3_RX_st.speed / MOTOR_SPEED_SCALE * refSign;
  ELD2_data.actualMode = PDO1_rx.PDO1_RX_st.mode;
  // send SYNC to update PDO data
  sendSYNC();
  // If drive command == clear fault state clear fault code
  if (ELD2_data.control.controlWordSt.flearFault) {
    ELD2_data.status.statusWord = 0;
    ELD2_data.errorCode = 0;
  }
  // If drive in fault state read actual fault code
  if (ELD2_data.status.statusWordSt.fault) {
    sendSDO(SDO_40, 0x603f, 0, 0);
  }
}

void ELD2CAN::rxSDOParse(struct can_frame *msg) {
  memcpy(&SDO_rx.sdo_data[0], msg->data, 8);
  switch (SDO_rx.SDO_st.index) {
    case 0x603f:  //  ERROR Code
      ELD2_data.errorCode = SDO_rx.SDO_st.data;
      break;
    case 0x6072:  //  Absolute torque limit
      ELD2_data.torqueLimitPercent = SDO_rx.SDO_st.data;
      break;
    default:
      break;
  }
}

void ELD2CAN::setCmdValue(uint16_t cmd) {
  localCmd = cmd;
  commandFlashBit = true;
}

void ELD2CAN::setRefValue(float ref) { localRef = ref; }

void ELD2CAN::setDirValue(int dir) { localDir = dir; }

void ELD2CAN::setLoopValue(uint8_t cmd) {
  if (cmd == 3) localLoop = TORQUE;
  if (cmd == 1) localLoop = SPEED;
  if (cmd == 2) localLoop = POSITION;
}

void ELD2CAN::setInfinityCommandMode() { cmdWaitCounter = CMDWAITINFINITY; }

// ELD2 recived message parsing
void ELD2CAN::rxParse(struct can_frame *msg) {
  // If any message from device with ID recived sat online state
  this->deviceOnline = 1;
  uint16_t msg_type_id = msg->can_id & (~getNodeId());
  switch (msg_type_id) {
    case SDO_R:
      rxSDOParse(msg);
      break;
    case PDO1_RX:
      memcpy(&PDO1_rx.pdo_data[0], msg->data, 8);
      break;
    case PDO2_RX:
      memcpy(&PDO2_rx.pdo_data[0], msg->data, 8);
      break;
    case PDO3_RX:
      memcpy(&PDO3_rx.pdo_data[0], msg->data, 8);
      break;
    case PDO4_RX:
      break;
    default:
      break;
  }
}

// Periodic time fuction with logic states
void ELD2CAN::deviceTimeTick(void) {
  if (this->getState() == NODE_NOTINIT) {
    this->deviceInit();
  }

  this->deviceCtrl();
}

ELD2CAN::LOOP_MODE_STATE ELD2CAN::getLoopValue(void) { return (localLoop); }

float ELD2CAN::getRefValue(void) { return localRef; }

uint32_t ELD2CAN::getCmdValue(void) { return localCmd; }

bool ELD2CAN::getDirValue(void) { return localDir; }

void ELD2CAN::makePDO1() { sendPDO(PDO1_TX, &PDO1_tx.pdo_data[0]); }
void ELD2CAN::makePDO2() { sendPDO(PDO2_TX, &PDO2_tx.pdo_data[0]); }
void ELD2CAN::makePDO3() { sendPDO(PDO3_TX, &PDO3_tx.pdo_data[0]); }
void ELD2CAN::makePDO4() { sendPDO(PDO4_TX, &PDO4_tx.pdo_data[0]); }